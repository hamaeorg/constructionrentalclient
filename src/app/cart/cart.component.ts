import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { RentService } from '../services/rent.service';
import { RentItemsService } from '../services/rent-items.service';
import { EquipmentService } from '../services/equipment.service';
import { InvoiceService } from '../services/invoice.service';
import {jsPDF} from 'jspdf';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.css']
})
export class CartComponent implements OnInit {

  rents = [];
  pendingRents = [];
  showPreviousRents : boolean = false;
  invoice;

  constructor(private rentService: RentService, private equipmentService: EquipmentService, private rentItemsService: RentItemsService, private invoiceService: InvoiceService) { }

  ngOnInit(): void {
    this.initRents();
    this.initPendingRents();
  }

  public initRents(): void {
    this.rentService.getAll("").subscribe((data: any[])=>{
      console.log(data);
      this.rents = data;
    })
  }

  public initPendingRents(): void {
    this.rentService.getAll("Open").subscribe((data: any[])=>{
      console.log(data);
      this.pendingRents = data;
    })
  }

  public deleteItem(rentItem): void{
      rentItem.isVisible = false;
      this.rentItemsService.put(rentItem).subscribe();
      this.rents = [];
      this.ngOnInit();
    }

  public cancelRent(rent): void{
    rent.status = "Cancelled"
    rent.rentItems = []
    this.rentService.put(rent).subscribe();
    this.rents = [];
    this.ngOnInit();
  }

  public confirmRent(rent){
    rent.status = "Confirmed"
    rent.rentItems = []
    this.rentService.put(rent).subscribe()
    this.rents = [];
    this.ngOnInit();
  }

  public generateInvoice(rent){
    this.rentService.getInvoiceByRentId(rent.rentId).subscribe((data: any[])=>{
    console.log(data);
    this.invoice = data;
    })
  }

  @ViewChild('pdfTable', {static: false}) pdfTable: ElementRef;

  // TODO: should definitely move this file generation to backend, and also learn proper javascript
  public downloadAsPDF() {
    const DATA = this.pdfTable.nativeElement;
      const doc: jsPDF = new jsPDF("p", "pt", 'a2', true);
      doc.html(DATA, {
         callback: (doc) => {
           doc.output("dataurlnewwindow");
         }
      });
  }

  public calculateRentDuration(rentItem): number{
    if (rentItem !== null){
      var date1:any = new Date(rentItem.startTime);
      var date2:any = new Date(rentItem.endTime);
      var diffDays:any = Math.floor((date2 - date1) / (1000 * 60 * 60 * 24));
      return diffDays;
    }
  }

  public togglePreviousRents() {
    if(this.showPreviousRents){
      this.showPreviousRents = false;
    } else {
      this.showPreviousRents = true;
    }
  }
  public resetInvoiceView() {
  this.invoice = null;
  }
}
