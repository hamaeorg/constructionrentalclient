import { Component, OnInit } from '@angular/core';
import { EquipmentService } from '../services/equipment.service';
import { RentService } from '../services/rent.service';
import { RentItemsService } from '../services/rent-items.service';
import { DateAdapter } from '@angular/material/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  equipments = [];
  rent : any;
  rentId: number;
  selectedEquipment;
  equipment;
  startTime: Date;
  endTime: Date;
  days: number = 0;

	constructor(private equipmentService: EquipmentService, private rentService: RentService, private rentItemsService: RentItemsService, private dateAdapter: DateAdapter<Date>)
	{
	  this.dateAdapter.setLocale('es-ES');
	}

  ngOnInit() {
    this.initRent();
    this.getEquipments();
    this.startTime = new Date();
    this.endTime = new Date();
  }

  getEquipments(): void {
    this.equipmentService.getAll().subscribe((data: any[])=>{
      console.log(data);
      this.equipments = data;
    })
  }

  initRent(): void {
    this.rentService.init().subscribe(data => {
      console.log(data);
      this.rent = data;
    })
  }

  postRentItem(rentItem): void {
    this.rentItemsService.post(rentItem).subscribe((data: any)=>{
      console.log(data);
    })
  }

  rentPending(equipment): void {
      if (this.selectedEquipment !== equipment) {
        this.selectedEquipment = equipment;
      } else {
        this.selectedEquipment = null;
        this.days = 0;
      }
  }

    public changeRentStartTime(event): void {
      this.startTime = event;
      this.reCalculateRentDays()
    }

    public changeRentEndTime(event): void {
      this.endTime = event;
      this.reCalculateRentDays()
    }

    public reCalculateRentDays(): void {
      this.days = this.calculateDiff(this.startTime, this.endTime)
    }

    public calculateDiff(firstDate, secondDate): number {
        return Math.floor((secondDate - firstDate) / (1000 * 60 * 60 * 24));
    }

    public createRentItem(): any {
      return {
           rentItemId: 0,
           startTime: this.startTime,
           endTime: this.endTime,
           equipmentId: this.selectedEquipment.equipmentId,
           rentId: this.rent.rentId
          }
    }

    public addNewPendingRent(): void {
      if(this.days > 0){
        if(!this.rent){
          this.initRent();
        }
        this.postRentItem(this.createRentItem())
        this.selectedEquipment = null;
        this.days = 0
      }
    }
}

