import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { throwError } from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {Status} from '../enums/status';

@Injectable({
  providedIn: 'root'
})
export class InvoiceService {

  private SERVER_URL = "api/invoices";
  private readonly httpOptions;

  constructor(private httpClient: HttpClient) {}

  handleError(error: HttpErrorResponse) {
      let errorMessage = 'Unknown error!';
      if (error.error instanceof ErrorEvent) {
        // Client-side errors
        errorMessage = `Error: ${error.error.message}`;
      } else {
        // Server-side errors
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }
      window.alert(errorMessage);
      return throwError(errorMessage);
  }

  public getByRentId(id): Observable<any>{
      return this.httpClient.get(`${this.SERVER_URL}/${id}`).pipe(catchError(this.handleError));
    }
}
