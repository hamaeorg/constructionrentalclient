import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class RentItemsService {

  private SERVER_URL = "api/rentitems";
  private readonly httpOptions;

  constructor(private httpClient: HttpClient) {}


  handleError(error: HttpErrorResponse) {
      let errorMessage = 'Unknown error!';
      if (error.error instanceof ErrorEvent) {
        // Client-side errors
        errorMessage = `Error: ${error.error.message}`;
      } else {
        // Server-side errors
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }
      window.alert(errorMessage);
      return throwError(errorMessage);
  }

  public getAll(){
    return this.httpClient.get(this.SERVER_URL).pipe(catchError(this.handleError));
  }

  public post(rentItem): Observable<Object>{
    return this.httpClient.post(this.SERVER_URL, rentItem).pipe(catchError(this.handleError));
    }

  public put(rentItem): Observable<Object> {
    return this.httpClient.put(`${this.SERVER_URL}/${rentItem.rentItemId}`, rentItem).pipe(catchError(this.handleError));
  }
}
