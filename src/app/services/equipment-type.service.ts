import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse  } from '@angular/common/http';
import {  throwError } from 'rxjs';
import {catchError } from 'rxjs/operators';
@Injectable({
  providedIn: 'root'
})

export class EquipmentTypeService {

  private SERVER_URL = "api/equipmentTypes";
  constructor(private httpClient: HttpClient) { }

  handleError(error: HttpErrorResponse) {
      let errorMessage = 'Unknown error!';
      if (error.error instanceof ErrorEvent) {
        // Client-side errors
        errorMessage = `Error: ${error.error.message}`;
      } else {
        // Server-side errors
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }
      window.alert(errorMessage);
      return throwError(errorMessage);
  }

  public getAll(){
      return this.httpClient.get(this.SERVER_URL).pipe(catchError(this.handleError));
    }
}
