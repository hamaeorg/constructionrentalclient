import { TestBed } from '@angular/core/testing';

import { RentItemsService } from './rent-items.service';

describe('RentItemsService', () => {
  let service: RentItemsService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(RentItemsService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
