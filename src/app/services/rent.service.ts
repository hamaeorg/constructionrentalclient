import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { throwError } from 'rxjs';
import {catchError} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {Status} from '../enums/status';

@Injectable({
  providedIn: 'root'
})
export class RentService {

  private SERVER_URL = "api/rents";
  private readonly httpOptions;

  constructor(private httpClient: HttpClient) {}

  handleError(error: HttpErrorResponse) {
      let errorMessage = 'Unknown error!';
      if (error.error instanceof ErrorEvent) {
        // Client-side errors
        errorMessage = `Error: ${error.error.message}`;
      } else {
        // Server-side errors
        errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
      }
      window.alert(errorMessage);
      return throwError(errorMessage);
  }

  public init(): Observable<any>{
    return this.httpClient.post(this.SERVER_URL, {}).pipe(catchError(this.handleError));
  }

  public getAll(status){
    return this.httpClient.get(`${this.SERVER_URL }/?status=${status}`).pipe(catchError(this.handleError));
  }

  public put(rent): Observable<Object> {
      return this.httpClient.put(`${this.SERVER_URL}/${rent.rentId}`, rent).pipe(catchError(this.handleError));
    }

  public getInvoiceByRentId(rentId): Observable<any>{
    return this.httpClient.get(`${this.SERVER_URL}/${rentId}/invoices`).pipe(catchError(this.handleError));
  }
}
