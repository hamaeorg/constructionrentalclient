export enum Status {
    Open,
    Confirmed,
    Cancelled,
    Settled
}
