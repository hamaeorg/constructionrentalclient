export enum PriceType {
    OneTime,
    PremiumDaily,
    RegularDaily
}
